# 23kHz Project - Make Dog Whistles Audible

23khz is a tool to help people reporting hate on the Internet by identifying
specific dog whistles without the reporter needing to explain them
each time.

Create a link to a report that lists and explains the specific dog whistles
you want to highlight when reporting an incident to a moderator.

# License

This 23khz Copyright Three Tablets LLC, available under the Apache 2.0 International License

Simple.css Copyright Kevin Quirk, available under the MIT License.
