from flask import g
from dataclasses import dataclass, field
from collections import defaultdict
from toml import load as toml_load

@dataclass
class Reason:
    key: str
    category: 'Category'
    form: str
    explanation: str
    more: str

    @property
    def sortable_form_key(self):
        sortable = self.form.replace('"', '')
        sortable = sortable.lower()
        return sortable

@dataclass
class Category:
    key: str
    form: str
    reasons: dict[Reason] = field(default_factory=dict)
    
@dataclass
class DB:
    categories: dict[Reason] = field(default_factory=dict)

    def get(self, key):
        """Get reason by full key

        Usage: db.get('category_key.reason_key')
        """
        cat_key, reason_key = key.split('.')
        if not cat_key in self.categories:
            return None
        return self.categories[cat_key].reasons.get(reason_key)


def get_db(entries='reasons.toml'):
    """Get the 'database' entries and attach them to the application"""
    if 'db' in g:
        # Nothing to do here
        return g.db
    categories = defaultdict(Category)
    reasons = defaultdict(Reason)
    with open(entries, 'r') as fd:
        d = toml_load(fd)
    for cat_key,cat_value in d.items():
        # The top level is categories
        category = Category(key=cat_key, form=cat_value['form'])
        for reason_key, reason_value in cat_value.items():
            if not isinstance(reason_value, dict):
                continue
            reason = Reason(key=reason_key, category=category,
                            **reason_value)
            category.reasons[reason_key] = reason
        categories[cat_key] = category
    return DB(categories=categories)
