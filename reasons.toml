[j]
form = "Antisemitism/Jew Hatred"

[j.bl]
form = "Blood Libel"
explanation = "Contains references to the Blood Libel Myth"
more = """
Blood Libel is an antisemitic idea that Jews murder Christians as part of their rituals. Ancient blood libel stories falsely claimed that Jews murdered Christian children to use their blood to make matzoh.
Modern blood libel ideas often employ imagery of Israeli soldiers seeking out Palestinian children to murder them.
"""

[j.ds]
form = "Double Standard"
explanation = "Applies a double standard to Jews and or the state of Israel"
more = """
One way in which antisemitism presents itself is by demanding that Jews take or refrain from actions that would be considered normal by others.
This is most often conveyed through the guise of "Israel", by demanding that Israel take actions that would not be expected of other countries, or by singling out Israel for criticism that could apply to many nations.
By applying this double standard exclusively to the Jewish state, these demands shift from criticism of Israeli policy to targeted harassment of Jews.

Double Standards towards Israel are explicitly spelled out in the [IHRA Definition of Antisemitism](https://www.holocaustremembrance.com/resources/working-definitions-charters/working-definition-antisemitism) as well as the [3D Test of Antisemitism](https://en.wikipedia.org/wiki/Three_Ds_of_antisemitism) and is tracked by the ADL's [Global 100 Hate Index](https://global100.adl.org/map)
"""

[j.co]
form = "Jewish control"
explanation = "Use of the 'Jewish control' claim"
more = """
The myth of Jewish control over world powers, including banks, politics, and media is largely rooted in the antisemitic book "The Protocols of the Elders of Zion", which invented and popularized this hateful myth.

The imagery used for this false claim comes in many forms, from secret societies, to "puppet masters", and today through the myth of Israeli political influence of foreign politicians and governments worldwide.
"""

[j.ca]
form = "Cabal"
explanation = "Uses the \"cabal\" claim"
more = """
The "cabal" trope is the idea that Jews are part of a secret group that controls the world, often through either banking or world politics. This trope was made famous in the antisemitic book "The Protocols of the Elders of Zion", and is expressed in many forms. In fact, the word cabal comes from the Jewish text "Kabbalah".
"""

[j.ct]
form = "Conspiracy Theory"
explanation = "The Conspiracy Theory claim"
more = """
The "Conspiracy Theory" claim blames Jews for tragedies around the world. This theory has been around since medieval times and has been used to blame Jews for everything from plague outbreaks, to earthquakes, terrorist events, and more. It's also used to blame Jews for violence by groups other than Jews, such as the theory that Jews are responsible for urban gangs in the United States
"""

[j.sh]
form = "Subhuman"
explanation = "The idea that Jews are sub-human"
more = """
Jews are often referred to as sub-human in antisemitic speech and literature. This may be in a literal form, such as calling Jews "Lizard People", or may present itself in more abstract forms, such as referring to Jews as "parasites", "leeches", or "termites", imagery that not only implies Jews are sub-human, but that they steal from non-Jews.
"""

[j.de]
form = "Deadly Exchange"
explanation = "The idea that Jews are responsible for police brutality or privacy invasions"
more = """
The Deadly Exchange myth contains the idea that Jews are responsible for police brutality, privacy invasions, or other civil rights violations through direct or indirect involvement by Israel. This often comes in the form of claims of IDF or Mossad training, or use of Israeli surveillance products.

While claims of Israeli involvement are not antisemitic on their own, when these claims are either untrue, or Israel is emphasized, it becomes clear that "Israel" is a dog whistle for "Jews" and that this claim is related to "Jewish Control"
"""

[j.ck]
form = "Christ Killer"
explanation = "The claim that Jews killed Jesus"
more = """
While the many Christian organizations, including the Catholic Church, have refuted the claim that Jews killed, and bear eternal responsibility for the death of Jesus Christ, many still hold on to this belief and claim that Jews are "Christ killers" or that they are the "Synagogue of Satan".

Modern representations of this hateful trope represent Jesus as an Arab Palestinian and Israelis/Jews as crucifying him.
"""

[j.cr]
form = "Collective Responsibility"
explanation = "Collective Responsibility/Collective Punishment"
more = """
Collective responsibility is the idea that any individual of a group should be held responsible for and punished alongside another member of the group.

In terms of antisemitism, this often comes either in the form of holding all Jews responsible for the actions of the Israeli government, or holding all Jews responsible for the percieved actions of Orthodox Jews.
"""

[j.mj]
form = "Identifying/marking Jews"
explanation = "Pointing out or identifying Jews for targets of harassment or violence"
more = """
A common antisemitic trope is to "identify" Jews and or to "mark" them for harassment or violence, for example by showing an image of the person with a Star of David above their head, or by using the "triple parentheses" around them, such as (((John Smith))). This is done to both incite further targeting or harassment, or to intimidate the target.

This trope is closely related to the idea of Jews as sub-human. By marking Jews, the speaker is making the claim that Jews are "stealth" and thus must be identified and defeated.
"""

[j.hd]
form = "Holocaust Denial"
explanation = "Holocaust Denial"
more = """
Holocaust denial is the idea that the holocaust was either a fabrication or was exaggerated. This is despite mountains of evidence demonstrating the full scope and impact of the Holocaust, including Nazi records, photographic evidence, and testimony from both victims and Nazi officers. Holocaust denial is used by antisemites to promote the idea that Jews are "professional victims" or that Jews are using the holocaust to garner sympathy.
Holocaust denial often comes in the form of "questioning the evidence" or claims that the numbers were "impossible, as well as antisemitic phrases such as "holohoax". Other forms of Holocaust denial include trivialization of the Holocaust through comparisons to lesser tragedies, or by "Nazi Comparisons", i.e. comparing a group to Nazis.
"""

[j.pp]
form = "Political Pawns"
explanation = "Using Jews as political pawns"
more = """
It's very common in modern antisemitism to claim that antisemitism is part of their political opposition's party. For example, someone associated with left-wing political ideology will claim that antisemitism is a right-wing phenomenon, and someone associated with right-wing political ideology will claim that antisemitism is a left-wing phenomenon.

Antisemitism is present on all ends of the political spectrum, and classifying antisemitism as a political issue, rather than as a systemic societal issue, acts to silence victims of Jew hatred and reduces Jews to partisan political pawns.
"""

[j.nc]
form = "Nazi Comparison"
explanation = "Comparing Jews to Nazis"
more = """
The Nazis killed almost two thirds of European Jews during their reign, the most Jews killed by any single attack. This period of history looms large in the minds of all Jews, including Sephardi and Beta Israeli Jews, who were largely not directly involved in the Holocaust.

While criticism of actions by Jews, and of Israel, does not in itself constitute antisemitism, using a comparison of Jews to Nazis, rather than any other totalitarian reigemes, is meant to re-traumatize Jews, and thus is antisemitic.
"""

[j.we]
form = "Jewish Wealth"
explanation = 'Contains the idea of "Jewish Wealth"'
more = """
There is an untrue stereotype that Jews are part of an elite or exclusive club of Jewish wealth and class. This idea often comes with other ideas, such as Jewish insularity, the idea that Jews obtain their wealth through exploitation of non-Jews, or that they use this wealth to exert control over politicians to further a "Jewish Agenda".

Those who use this claim often use one of many dog whistles to avoid appearing antisemitic, such as using terms like "east coast", "elite", "cosmopolitan", "globalist", "Soros", or "Rothschild".
"""

[j.rj]
form = "Real Jews"
explanation = "The idea that Jews are not the \"Real Jews\""
more = """
The idea that the millions of Jews around the world are not the "Real Jews", but are simply posing as Jews.
"""

[j.op]
form = "Visibly Jewish"
explanation = "Prejudice against the \"visibly Jewish\""
more = """
Antisemitism has many forms, one of which is claiming that there are "Good Jews" who look/act like gentiles and "Bad Jews" who act/dress/speak differently and refuse to be part of modern society.

This is hateful because it discriminates against a group of people based solely on their religious beliefs and how they dress.
"""

[j.zi]
form = "\"Zionist\""
explanation = "Using \"Zio\" or \"Zionist\" as code for Jew"
more = """
While there is room for political discourse on Zionism, modern antisemitism often uses terms like "Zionist" instead of Jew because it is considered too overt. Therefore, many antisemites use terms like "Zionist" or "Zio" (a phrase coined by David Duke), or even "Israel" instead of "Jew" to hide their hateful rhetoric.
"""

[j.jc]
form = "Decentering"
explanation = "Decentering Jews in conversations around antisemitism"
more = """
During conversations about antisemitism, a common antisemitic tactic is to bring up other people, often Palestinians, during conversations around hate.

This tactic focuses the shift away from Jew hatred, and also introduces Collective Responsibility/Collective Punishment
"""

[j.se]
form = "Semites"
explanation = "Arguing that Jews aren't/aren't the only \"Semites\""
more = """
The term "antisemitism" refers to Jew hatred. Some antisemites will claim that other groups, such as Arabs, are also "Semites" and thus Jew hatred cannot be antisemitism.

The term "Semitic" refers to a language group, not an ethnic group. Calling all people of the Levant "Semites" in order to erase Jewish ethnic identity is a form of misinformation and Jew hatred.
"""

[j.gj]
form = "Good Jews"
explanation = "The idea that there are \"Good Jews\" and \"Bad Jews\""
more = """
One antisemitic trope is the idea of "Good Jews" and "Bad Jews", with "Bad Jews" being Israeli Jews, or Zionist Jews. The idea that can classify people as good or bad based on their nationality or philosophical ideas around self-sovernenty is a form of hate.
"""

[j.fp]
form = "Destroy Israel"
explanation = "Calls for the Destruction of Israel"
more = """
Calling for the destruction of Israel is a common modern dog whistle. Since Israel holds roughly half the world's Jews, calling for its destruction serves the function of calling for the death of most Jews without needing to call for the death of Jews directly.
"""

[t]
form = "Transphobia"

[t.ds]
form = "Double Standards"
explanation = "Double standards to be seen as equal to cis people of the same gender"
more = """
Trans people are usually held to double standards higher than cis people in order to be considered equals.
For example, a trans woman often has to fit within a very narrow standard of femininity in order to be seen as a woman, despite many cis women not fitting within that standard and still being seen as women.
Often people will say it is reasonable to misgender a trans person for not fitting within gender norms (and especially non-binary people, who cannot do this), when they gender cis people correctly with no issue without doing this. Cis people who say such things are applying double standards to blame trans people for their own discomfort with being misgendered.
"""

[t.se]
form = "Side Effects"
explanation = "Claiming that puberty blockers have irreversible side effects and are dangerous"
more = """
A common talking point against puberty blockers, which are commonly prescribed to cis children who go through early puberty without controversy, is that they cause irreversible side effects.
The truth is that numerous scientific studies show that puberty blockers in themselves do not have irreversible side effects, and people who use this as a talking point are faking concern in order to hide malicious transphobia, and a desire to get in between the care that they and their medical care team has decided is healthiest and best for them.
"""

[t.bu]
form = "Bullies"
explanation = "Claiming that trans people are bullies and too pushy"
more = """
A common tactic used against trans people advocating for themselves in any way a cis person would is to refer to them as bullies, being pushy, or being mean, For example, “trans people are being pushy by making me use their pronouns”, or someone deciding that the trans person is the bully of the situation for having a rude response after being misgendered.
"""

[t.tg]
form = "Two Genders"
explanation = "Claiming that there are only two genders"
more = """
Gender is not binary, this is a basic sociological observation and fact observed over the course of recorded human history. See https://nonbinary.wiki/wiki/History_of_nonbinary_gender for more information including citations.
"""

[t.bs]
form = "Biological Sex"
explanation = "Claiming trans people aren't their \"real\" genders or biological sex"
more = """
Transphobes will often say that trans people’s gender is a delusion, mental illness, or some sort of symptom and not a real phenomenon on the level of cis people’s.

Other examples of this are trying to say that trans people aren’t “biologically” their gender because it doesn’t match their “biological sex”, when number one, trans people’s genders are a part of their biology, and number two, trans people can also change most parts of their biological sex. This isn’t a real or factual statement, and is simply a way of misgendering trans people while trying to hide behind “science”.
See https://blogs.scientificamerican.com/voices/stop-using-phony-science-to-justify-transphobia/ for more information.
"""

[t.dw]
form = "Dangerous to Women"
explanation = "Claiming trans women are dangerous and could be triggering in women’s spaces"
more = """
This is a dog-whistle used to depict trans women as predators who are going to cause harm in women’s spaces, or based on the logic that trans women having penises (despite not all trans women having such) may trigger women in some spaces such as rape support groups (despite the need of trans women needing such groups, and the rate of trans women being raped being higher than the rate for cis women).
Additionally, the assumption that trans women will act in this way is based in assuming that trans women share more similarities with men, and specifically predatory men, than other women, due to a view that trans women aren’t real women.
"""

[t.ep]
form = "Escaping Patriarchy"
explanation = "Claiming that trans men are just trying to escape sexism"
more = """
Many anti-trans cis people (such as JK Rowling) have used the idea of a woman transitioning to escape misogyny as a scapegoat to argue for restricting the necessary and life-saving medical care of trans men.
"""

[t.dc]
form = "Dangerous to Children"
explanation = "Claiming trans people are dangerous to children or that being trans is sexual"
more = """
Trans people are no less or more sexual than cis people and are equally safe for work as cis people. Just as cis people have the capacity to act in adult-only contexts, so do trans people.

Being trans is not a contagion and will not spread to children, and increased numbers of children identifying as trans in recent years is explained due to increased information, acceptance, and social resources.
"""

[t.fo]
form = "Fourty One"
explanation = "Jokes about 41, related to trans suicide attempt statistics"
more = """
These jokes may seem strange and random to the cis observer, but they are joking about the trans suicide attempt rate.
"""
